<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	function site_url($path = '')
	{
		if (is_array($path))
		{
			$path = implode('/', $path);
		}
		
		$path = config_item('base_url') . $path;
		
		return (substr($path, -1) == '/') ? $path : $path . '/';
	}
	
	function current_url()
	{
		$Base =& get_instance();
		return site_url($Base->router->uri_string());
	}
	
	function uri_string()
	{
		$Base =& get_instance();
		$uri = $Base->router->uri_string();
		
		return (substr($uri, -1) == '/') ? $uri : $uri . '/';
	}
	
	function anchor($path, $text = NULL, $attributes = NULL)
	{
		$text = (string) $text;
		if (empty($text))
		{
			$text = $path;
		}
		
		if (is_array($attributes))
		{
			$attr = '';
			foreach ($attributes as $attribute => $value)
			{
				$attr .= $attribute . '="' . $value . '" ';
			}
		}
		else
		{
			$attr = (string) $attributes;
		}
		
		return '<a href="' . site_url($path) . '" ' . $attr . '>' . $text . '</a>';
	}
	
	function redirect($path, $code=302)
	{
		header('Location: ' . site_url($path), TRUE, $code);
		die();
	}
?>
