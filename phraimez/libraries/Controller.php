<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	//Any public function declared here _MUST_ be added to the check in index.php!
	class Controller
	{
		private $autoload_libraries = array();
		private static $instance;
		
		public function __construct()
		{
			self::$instance =& $this;
			$this->_initialize();
		}
		
		public static function &get_instance()
		{
			return self::$instance;
		}
		
		private function _initialize()
		{
			//Let's give the controllers access to all the already loaded objects.			
			$this->router =& load_class('Router');
			$this->load =& load_class('Loader');
			
			//And any libraries that should be autoloaded.
			foreach ($this->autoload_libraries as $nickname => $class)
			{
				$this->$nickname =& load_class($class);
			}
			
			//Now everything will be available for the controller, adressed as $this->foo->do_stuff()
			
		}
	}
	
	//With this we can get the instance from outside the controller. (Most notably in libraries)
	function &get_instance()
	{
		return Controller::get_instance();
	}
	
?>
