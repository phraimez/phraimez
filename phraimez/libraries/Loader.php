<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	class Loader
	{
		private $loaded_libraries = array();
		private $loaded_models = array();
		private $loaded_helpers = array();
		
		public function library($library, $name = NULL)
		{
			$library = (string)$library;
			if (empty($library) || in_array($library, $this->loaded_libraries))
			{
				//We need a library not already loaded.
				return FALSE;
			}
			
			if (file_exists(APP_PATH . 'libraries/' . $library . '.php'))
			{
				$library_path = APP_PATH . 'libraries/' . $library . '.php';
			}
			elseif (file_exists(SYS_PATH . 'libraries/' . $library . '.php'))
			{
				//If it isn't in the APP/libraries it might be in SYS/libraries
				$library_path = SYS_PATH . 'libraries/' . $library . '.php';
			}
			else
			{
				die($library . ' couldnt be found in the library-directories.');
				return FALSE;
			}
			
			//Okay, the library seems to exist and have not been loaded already!
			require $library_path;
			$this->_instantiate_class($library, $name);
			
			$this->loaded_libraries[] = $library;
			return TRUE;
		}
		
		public function model($model, $name = NULL)
		{
			$model = (string) model;
			if (empty($model) || in_array($model, $this->loaded_models))
			{
				//We can't have a loaded model, or a model without name.
				return FALSE;
			}
			
			if (!file_exists(APP_PATH . 'models/' . $model . '.php'))
			{
				die($model . '.php couldnt be found around the other models');
				return FALSE;
			}
			
			if (!class_exists('Model'))
			{
				load_class('Model', FALSE);
			}
			
			require APP_PATH . 'models/' . $model . '.php';
			$this->_instantiate_class($model, $name);
			
			$this->loaded_models[] = $model;
			
		}
		
		//We are naming the parameters this weird, so we can avoid nameclashes
		//as far as possible with the the extracted variables.
		public function view($_view, $_data = NULL)
		{
			if (!file_exists(APP_PATH . 'views/' . $_view . '.php'))
			{
				die($_view . '.php is not in the views-folder.');
				return FALSE;
			}
			
			if (is_array($_data))
			{
				extract($_data, EXTR_SKIP); //This sure is PHP.
			}
			unset($_data);
			
			include APP_PATH . 'views/' . $_view . '.php';
		}
		
		public function helper($helper)
		{
			$helper = (string) $helper;
			
			if (in_array($helper, $this->loaded_helpers))
			{
				return FALSE;
			}
			
			if (file_exists(APP_PATH . 'helpers/' . $helper . '.php'))
			{
				require APP_PATH . 'helpers/' . $helper . '.php';
			}
			elseif (file_exists(SYS_PATH . 'helpers/' . $helper . '.php'))
			{
				require SYS_PATH . 'helpers/' . $helper . '.php';
			}
			else
			{
				die('The helper ' . $helper . ' was not found');
			}
			
			$this->loaded_helpers[] = $helper;
		}
		
		private function _instantiate_class($class, $name)
		{
			//This function has no chance what-so-ever to guess the CASE of the class,
			//send it with the right case or get dead!
			
			if (!class_exists($class))
			{
				die('The class named ' . $class . ' could be found.');
			}
		
			$name = empty($name) ? $class : $name;
			
			$Base =& get_instance();
			
			if (isset($Base->$name))
			{
				//We don't want to overwrite anything.
				die('The name ' . $name . ' is already taken.');
			}
			
			$Base->$name = new $class;
		}
	}
?>
