<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	class Router
	{
		private $default_controller = 'start';	//What to serve if no controller is mentioned
		private $default_method = 'index';	//What to serve if no method is mentioned
		
		private $uri_string;
		private $segments = array();
		
		public $controller;
		public $method;
		
		public function __construct ()
		{
			$this->_fetch_uri_string();
			$this->_set_routing();
		}
		
		public function segment($n)
		{
			return (isset($this->segments[$n])) ? $this->segments[$n] : FALSE;
		}

		public function segment_array()
		{
			return $this->segments;
		}

		public function total_segments()
		{
			return count($this->segments);
		}

		public function uri_string()
		{
			return $this->uri_string;
		}

		private function _set_routing()
		{

			if ($this->uri_string == '')
			{
				$this->controller = $this->default_controller;
				$this->method = $this->default_method;
				return;
			}

			$this->_explode_segments();
			$this->_validate_request();
			
			if ($this->total_segments() == 0)
			{
				$this->controller = $this->default_controller;
				$this->method = $this->default_method;
				return;
			}
			
			$this->controller = $this->segments[0];
			
			if (isset($this->segments[1]))
			{
				$this->method = $this->segments[1];
			}
			else
			{
				$this->method = $this->default_method;
			}
						
			$this->_reindex_segments();

			return;
		}

		private function _fetch_uri_string()
		{
			if (is_array($_GET) && count($_GET) == 1 && trim(key($_GET), '/') != '')
			{
				$this->uri_string = key($_GET);
				return;
			}

			$path = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : getenv('PATH_INFO');
			if (trim($path, '/') != '' && $path != '/index.php')
			{
				$this->uri_string = $path;
				return;
			}

			$path =  (isset($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : getenv('QUERY_STRING');
			if (trim($path, '/') != '')
			{
				$this->uri_string = $path;
				return;
			}

			$path = str_replace($_SERVER['SCRIPT_NAME'], '', (isset($_SERVER['ORIG_PATH_INFO'])) ? $_SERVER['ORIG_PATH_INFO'] : getenv('ORIG_PATH_INFO'));
			if (trim($path, '/') != '' && $path != '/index.php')
			{
				// remove path and script information so we have good URI data
				$this->uri_string = $path;
				return;
			}

			$this->uri_string = '';
			return;
		}

		private function _explode_segments()
		{
			foreach(explode("/", preg_replace("|/*(.+?)/*$|", "\\1", $this->uri_string)) as $val)
			{
				// Filter segments for security
				$val = trim($this->_filter_uri($val));

				if ($val != '')
				{
					$this->segments[] = $val;
				}
			}
		}
		
		private function _filter_uri($str)
		{
			$pattern = '|^[a-z 0-9~%\.\:_\-]+$|i';

			if (!preg_match($pattern, $str))
			{
				die('Invalid URI-segment.');
			}

			// Convert programatic characters to entities
			$bad	= array('$', 		'(', 		')',	 	'%28', 		'%29');
			$good	= array('&#36;',	'&#40;',	'&#41;',	'&#40;',	'&#41;');

			return str_replace($bad, $good, $str);
		}

		private function _reindex_segments()
		{
			array_unshift($this->segments, NULL);
			unset($this->segments[0]);
		}
		
		private function _validate_request()
		{
			if (file_exists(APP_PATH . 'controllers/' . $this->segments[0] . '.php'))
			{
				return;
			}
		
			die('Cant find ' . $this->segments[0] . '.php');
			return false;
		}
	}
?>
