<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	class Database
	{
		private $link;
		
		public function __construct()
		{
			//If you do not want to connect on startup, remove the following line.
			$this->connect();
		}
		
		public function connect($srv=NULL, $uname=NULL, $pass=NULL, $db=NULL)
		{
			$srv	= is_null($srv)		? 'localhost'	: $srv;
			$uname	= is_null($uname)	? 'root'		: $uname;
			$pass	= is_null($pass)	? 'password' 	: $srv;
			$db		= is_null($db)		? 'database'	: $db;
			
			$this->link = mysql_connect($srv, $uname, $pass);
			
			if (!$this->link)
			{
				return FALSE;
			}
			
			return mysql_select_db($db);
		}
		
		public function disconnect()
		{
			return mysql_close($this->link);
		}
		
	}
?>
