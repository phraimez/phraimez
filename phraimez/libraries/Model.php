<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	class Model
	{
		private $my_name;
		
		public function __construct()
		{
			$my_name = get_class($this);
			$this->_migrate_libraries();
		}
		
		//This function gives the model access to every object through $this, just as a controller.
		//Perhaps I'm just to tired to see a more obvious solution. (extending Controller?)
		private function _migrate_libraries()
		{
			//Every object we wish to give to model is on the behemoth object.
			
			$Base =& get_instance();
			
			foreach (get_object_vars($Base) as $name => $obj)
			{
				//We don't want to add something already there,
				//and we don't want to add the model to the model, cause that would be weird.
				if (!isset($this->$name) && $name != $this->my_name)
				{
					$this->$name =& $obj;
				}
			}
		}
	}
?>
