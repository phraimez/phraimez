<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 

	/*
	 * Allows you to access classes, whether already instantiated or not.
	 * 
	 * Usage:
	 * $a = load_class('firetruck');
	 * $a->do_stuff();
	 */ 
	function &load_class($class, $instant = TRUE)
	{
		//We don't want to initiate the same class twice...
		static $obj = array();
		
		//Lets not do the work again if it is already loaded.
		if (isset($obj[$class]))
		{
			return $obj[$class];
		}
		
		if (file_exists(APP_PATH . 'libraries/' . $class . '.php'))
		{
			require APP_PATH . 'libraries/' . $class . '.php';
		}
		else
		{
			require SYS_PATH . 'libraries/' . $class . '.php';
		}
		

		$obj[$class] = $instant ? instantiate_class(new $class()) : TRUE;
		
		return $obj[$class];
		//Some classes doesn't need to be instantiated. As an example: Controller has only one purpose:
		//to be extended by user-created controllers. Therefore there is no idea to have an instance of it.
		//We still want to save it in our static array though, so we doesn't load it twice!
	}
	
	/*
	 * Does some magic with the awkwardness of PHP
	 */
	function &instantiate_class(&$c)
	{
		return $c;
	}
	
	/*
	 * Loads the config file.
	 */
	function &load_config()
	{
		static $conf;
		 
		if (!isset($conf))
		{
			if (!file_exists(APP_PATH . 'config/config.php'))
			{
				die('There is no APP/config/config.php');
			}
			
			require APP_PATH . 'config/config.php';
			
			if (!isset($config) || !is_array($config))
			{
				die('Malconfiguration in APP/config/config.php');
			}
			
			$conf = $config;
		}
		
		return $conf;
	}
	
	/*
	 * Gives you the value of a config_item
	 */
	function config_item($item)
	{
		static $config_item = array();
		
		if (!isset($config_item[$item]))
		{
			$config =& load_config();
			$config_item[$item] = (isset($config[$item])) ? $config[$item] : FALSE;
		}
		
		return $config_item[$item];
	}
	

?>
