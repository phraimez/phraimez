<?php
	class Start extends Controller
	{
		public function index()
		{
			$this->load->helper('URL');	//This gives us access to the useful functions in the URL helper
			
			$data = array(
				'paragraph' => 'I hope you will like the framework, but you must remember. Case is important!',
				'anchor'	=> anchor(uri_string(), current_url(), array('title' => 'Yes, indeed!')),
				'time'		=> date('Y-m-d H:i:s'),
			);
			$this->load->view('heading', $data);
		}
	}
?>
