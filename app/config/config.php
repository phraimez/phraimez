<?php
	/*
	 * The error reporting level used, should probably be E_ALL in a development environment,
	 * but should probably be 0 or E_ERROR in a productive environment.
	 * 
	 * See php.net/manual/en/function.error-reporting.php for more info.
	 */
	$config['error_level'] = -1; //-1 means _everything_, including E_STRICT
	
	
	/*
	 * URL to the root, this will usually be your base URL _without_ a trailing slash.
	 */
	$config['base_url'] = 'http://localhost';
	
?>
