<?php
	/*
	 *      Phraimez Copyright 2010 Simon Johansson <simon@forintens.se>
	 *      
	 *      This file is part of Phraimez
	 * 
	 *      Phraimez is free software: you can redistribute it and/or modify
	 *      it under the terms of the GNU General Public License as published by
	 *      the Free Software Foundation, either version 3 of the License, or
	 *      (at your option) any later version.
	 *      
	 *      Phraimez is distributed in the hope that it will be useful,
	 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *      GNU General Public License for more details.
	 *      
	 *      You should have received a copy of the GNU General Public License
	 *      along with Phraimez. If not, see <http://www.gnu.org/licenses/>.
	 */
	 
	//We will start with defining some important constants, some important functions and set the error_reporting level.
	define('SYS_PATH', realpath(dirname(__FILE__)) . '/phraimez/');
	define('APP_PATH', realpath(dirname(__FILE__)) . '/app/');
	
	require_once SYS_PATH . 'atoms.php';
	
	ini_set('error_reporting', config_item('error_level'));
	
	
	//We will load Router, that library will help us with determing which controller to load.
	$ROUTER = load_class('Router');
	
	
	//Load the Controller class, which will be extended by the Router-specified controller.
	load_class('Controller', FALSE);
	
	$class = $ROUTER->controller;
	$method = $ROUTER->method;
	
	if (!file_exists(APP_PATH . 'controllers/' . $class . '.php'))
	{
		die('No controller named ' . $class);
	}
	
	require APP_PATH . 'controllers/' . $class . '.php';
	
	if (!class_exists($class) || $method == 'get_instance')
	{
		//Here we are checking for that the controller-class exists, and also for every
		//public function in the Base-controller!
		die('404 Page Not Found');
	}
	
	$CONTROLLER = new $class();
	
	//I had some fun debugging time here until I noticed that
	//is_callable is not working really as you think it should.
	//Had to do this ugly work-around.
	if (!in_array($method, get_class_methods($CONTROLLER)))
	{
		die('No method named ' . $method);
	}
	
	call_user_func_array(array(&$CONTROLLER, $method), array_slice($ROUTER->segment_array(), 2));
	//Deprecated since long ago. Hard to find a workaround though, eval is the option which should
	//always be avoided. Simplest would be to have all controller-functions take an array as the only parameter.
	//But that would also be ugly. =/
	
?>
